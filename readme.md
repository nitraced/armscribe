# Projet Armscribe

Ce projet a été réalisé dans le cadre du cours Analyse et Commande des systèmes non-linéaires donné par le Pr. Guchuan Zhu à Polytechnique Montréal.

## Description

Armscribe est un bras doté de deux liaisons pivot (Rotoïde-Rotoïde ou RR) qui peut écrire sur un tableau. Le projet consiste en la génération de primitives d'écriture paramétrées en vitesse suivant des lois Delta-Lognormales, à l'intar de l'être humain. Notre étude porte sur le design d'un contrôleur non-linéaire permettant au mieux de les suivres.

## Contributeurs

- Justin Cano, étudiant au Ph.D. , DGE Polytechnique Montréal.
- Titouan Le Marec, étudiant à la Maîtrise, DGE, Polytechnique Montréal.

## Contenu du répertoire

- `document_initial/` document de présentation brève du projet pour son lancement;
- `rapport/` un rapport consignant notre démarche et nos résultats;
- `assets/` des *slides* et des vidéos montrant les résultats du projet de simulation;
- `modelisation/` les codes de modélisation, synthèse et validation en Matlab; les scripts de calcul formel en Python et les figures schèmatiques effectuées sur Dia. 

## Configuration logicielle

Ubuntu 18.04 LTS avec Matlab 2018a, la *robotics toolbox* de Peter Corke est requise mais est téléchargeable gratuitement (les codes nécessaires sont déjà dans ce répertoire.).

