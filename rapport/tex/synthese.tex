\section{Synthèse des contrôleurs}
\label{s:controleurs}

\subsection{Signaux de référence}
\label{ss:motionplanner}
\subsubsection{Physiologie des primitives d'écriture}
Les primitives des mouvements d'un bras humain pour l'écriture sont données en vitesse par une loi dite delta-Lognormale \cite{chidami_delta-lognormal_2018} :

\[ |v(t-t_0)| = D( \Lambda_1(t) - D_2 \Lambda_2(t)), \]

où 
\[ \Lambda_i = 
\begin{cases}
\left[ \sigma_i \sqrt{2\pi} (t-t_0^i) \right]^{-1} \exp \left( -\frac{ [\ln(t-t_0)-\mu_i]^2}{2\sigma_i^2} \right)  \text{ si } t > t_0^i, \\
0 \text{ sinon},
\end{cases}
\]

avec les constantes définies comme :
\begin{description}
\item [$D_i$ :] amplitudes des commandes musculaires;
\item [$t_0^i$ :] temps de délai (neuronal);
\item [$\mu_i$ :] date de pic logarithmique de la lognormale;
\item [$\sigma_i$ :] temps de log-réponse.   
\end{description}



La log-normalité est expliquée par l'application du théorème Centrale-Limite sur des fibres musculaires en cascade et le caractère soustracteur est dû au fait que la physiologie du corps humain inclut des structures bimusculaires \cite{ferrer}. En ce qui concerne le bras, il est doté d'un muscle agoniste (dans le sens de la direction du mouvement) et d'un muscle antagoniste (dans le sens inverse) qui effectuent respectivement une contraction et une dilatation, et vice-versa suivant la direction du mouvement que souhaite prendre le bras. Nous pouvons voir une illustration de la vitesse tangentielle lors d'une primitive (\textit{stroke}) d'écriture prise par un muscle humain à la figure \ref{fig:plamondon}. 

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{fig/plamondon.jpeg}
\caption{Courbe de vitesse tangentielle delta-lognormale sur un muscle réel, tiré de \cite{plamondon1995kinematic}.}
\label{fig:plamondon}
\end{figure}

\subsubsection{Simplification du paramétrage pour notre robot}
Nous supposerons que le muscle agoniste correspond à l'indice 1 et le muscle antagoniste au 2 respectivement.

Nous aurons besoin de longueur de liberté pour générer des signaux. En effet, notre problème est un parcours de trajectoire curviligne (les primitives) $\mbf p_{obj} (t) \in \mathcal{C}$ où $\mathcal{C}$ est la courbe de la primitive d'écriture. Cette primitive est de \textbf{longueur} totale $L$ et sera parcourue en un \textbf{temps} $T$ par l'outil d'écriture en $\mbf p(t)$ (implicitement cette deuxième variable est reliée à la vitesse moyenne de parcours $\bar{v}:=L/T$). La position sur la courbe de $\mbf p(t)$ nous donne l'abscisse curviligne $l(t)$ et sa vitesse est la vitesse tangentielle $v(t)=\dot{l}(t) = \sqrt{\dot{x}^2+\dot{z}^2}$. 

La forme de la loi lognormale est gouvernée principalement par $\sigma$ en effet, nous pouvons tracer pour divers sigma une loi lognormale $\mu = 1; t_0 = 0; D=1$ comme ceci fut fait à la figure \ref{fig:lognormal-sigma}. Nous pouvons constater que, à l'instar d'une gaussienne, l'étroitesse de cette dernière est fonction inverse de $\sigma$. Toutefois lorsque $\sigma$ est grand, la fonction inverse au dénominateur a tendance à l'emporter pour $t \rightarrow 0$. 

Nous prendrons donc une valeur constante de $\sigma$ pour chacun des mouvement afin simplifier et nous la choisirons judicieusement afin d'effectuer une accélération progressive (et limiter les efforts des effecteurs).

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{fig/lognormal_sigma.eps}
\caption{Tracé de l'évolution des formes d'une loi lognormale en fonction de $\sigma$, tiré de \cite{lognormale}. }
\label{fig:lognormal-sigma}
\end{figure}


Afin de simplifier la loi lognormale pour la planifier et génèrer des signaux ressemblant à ceux produits par un être humain en pratique \cite{plamondon1995kinematic} nous prendrons des valeurs autour de $\sigma = \frac{1}{2}$. Nous supposerons que le muscle agoniste a invariablement $\sigma_1 := 1/2$ comme paramètre et le muscle antagoniste une valeur 20\% plus élevée $\sigma_2 := 1.2 \times \frac{1}{2}$. Cela s'explique physiologiquement par une action plus brutale (\textit{eg} en pic) du muscle antagoniste qui arrête le mouvement du bras au point donné. 

Nous supposerons l'amplitude de l'action du muscle antagoniste égale à 30\% du muscle agoniste, en l’occurrence $D_2 := 0.3$. 

Dernière chose à paramétrer, la date de pic logarithmique et les temps de commande musculaires. On supposera que ces dates sont identiques mais que le deuxième signal est décalé de la moitié de cette date. En prenant $\alpha$ comme un paramètre réel positif on fixe:
\[ \mu_1 = \mu_2 = \ln(\alpha),~ t_0^i = 0 \text{ et } t_0^2 = \frac{\alpha}{2}. \]
Le deuxième paramètre à ajuster est $D$. La forme de la fonction de vitesse pour l'abscisse curviligne est donc de la forme avec le doublet $\{D,\alpha\}$ comme paramètre d'ajustement :

\begin{equation}
v(t) = D \times  f(t;\alpha), \text{ où } f(t;\alpha) = \Lambda_1(t;\alpha) -0.3 \Lambda_2(t;\alpha),
\label{eq:lognormal}
\end{equation}
notons que nous pouvons voir l'évolution de $f(t;\alpha)$ en fonction de son paramètre $\alpha$ à la figure \ref{fig:v_fct_alpha}.
\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{fig/v_fct_alpha.eps} 
\caption{Évolution de $f(t;\alpha)$ en fonction de différentes valeurs du paramètre $\alpha$.}
\label{fig:v_fct_alpha}
\end{figure}



\subsubsection{Paramètres à ajuster pour une spline lognormale}

\paragraph{Contraintes du parcours :}
On a l'intervalle temporel de parcours de la primitive $[t_0,t_f]$ comme suit $t_0 + T = t_f$. Par conséquent il faut que $L = \int_{t=t_0}^{t_f} v(t)dt$ et que $v(t_0)=v(t_f)=0$ : chaque primitive de longueur $L$ est parcourue en un temps $T$ avec arrêt en au début et à la fin de cette dernière. 

\paragraph{Temps du parcours :}
Supposons que la courbe $\mathcal{C}$ soit donnée, on connaît donc $L$, on fixera une vitesse moyenne désirée de parcours à $\bar{v} = 0.1 m/s$, nos intervalles seront donc de $T=\frac{L}{\bar{v}}$.

\paragraph{Point d'arrêt :}
Tout d'abord, on fixera le paramètre $\alpha$ dans (\ref{eq:lognormal}) qui agit en dilatant la courbe et \textit{in fine} déterminant la valeur $t^*$ où $v(t^*,\alpha^*)=0$. Sans perte de généralités on supposera que $t_0 = 0$ (on applique le changement de variable $v(t-t_0)$) et $v(0)=D f(0) = 0$.

On trouvera donc une valeur particulière de $\alpha$ pour chacune des primitives telle que si $D=1$ alors $v(t_f;\alpha^*)=0$. On tâchera de minimiser le résidu comme suit :

\[ \alpha^* =\underset{\alpha \in \mathbb{R}^+}{\text{argmin}}   f^2(t_f;\alpha) , \] 

on prendra en guise d'estimé initial $\alpha^0 = 3t_f$ car cette valeur est proche de la valeur d'annulation en pratique. Une méthode résiduelle des moindres carrés de type Levenberg est appliquée en implémentation pour résoudre ce problème.

\paragraph{Longueur :} Une fois ceci fait, on peut évaluer numériquement la valeur finale de l'abscisse curviligne à l'aide de l'intégrale suivante :
\[ \tilde{L} = \int_{t=0}^{t_f} f(t;\alpha^*)dt \]
en implémentation, nous utilisons une méthode d'intégration à pas variable. Ensuite il suffit de trouver $D$ tel que $D\tilde{L}=L$ c'est-à-dire $D = \frac{L}{\tilde{L}}$ et le paramétrage log-normal est réalisé.


\subsubsection{Parcours curviligne de primitives}


Dans ce projet, nous considérons deux types de primitives d'écriture que nous pouvons voir à la figure \ref{fig:primitives}. Une \textbf{primitive segment} est une primitive linéaire allant d'un point quelconque $\mbf p_0$ de l'espace de travail à un point quelconque $\mbf p_F$ différent du premier. 

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{fig/primitives.pdf}
\caption{Types de primitives considérées.}
\label{fig:primitives}
\end{figure}
On note l'abscisse curviligne $l(t)=\int_{\sigma=0}^t v(\sigma) d \sigma$. Dans ce cas la consigne en position et la vitesse vaudront respectivement  :
\[ \mbf p_{obj}(t) = \frac{l(t)}{L} (\mbf p_F - \mbf p_0) \text{ et } \dot{\mbf p}_{obj}(t) = v(t) \frac{\mbf p_F - \mbf p_0}{L}, \] 

Une primitive \textbf{arc de cercle} verra la définition d'une matrice de rotation $\mbf R(\theta)$ d'angle $\theta(t) = \theta_F l(t)/L$ où $\theta_F = \measuredangle (\bp_0, \bp_C, \bp_F)$ \footnote{Remarquons que le cas présenté à la figure \ref{fig:primitives} est celui où $\theta=\theta_F$.} avec $\bp_c$ le centre de l'arc de cercle. On notera que $\dot{\mbf R}(\theta) = \theta_F v(t)\mbf R(\theta+\pi/2)/L $ par conséquent, on peut paramétrer les positions et les vitesse désirées dans l'espace de travail comme suit :
\[ \bp_{obj}(t) = \bp_c + \mbf R (\theta(t)) (\bp_0 -\bp_C) \text{ et } 
\bdp_{obj}(t)= \frac{\theta_F v(t)}{L} \mbf R(\theta(t) + \pi/2)  (\bp_0 -\bp_C).
 \]

\subsubsection{Séquencement d'un texte}

Afin de procéder à des tests concluants, nous avons décidé de séquencer un texte en plusieurs segments, nous menons les tests de validation sur le texte qui suit à la figure \ref{fig:trajtypique}.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{fig/poly.pdf}
\caption{Trajectoire typique séquencée utilisée pour la démonstration.}
\label{fig:trajtypique}
\end{figure}

Nous noterons trois types de séquence de texte \texttt{Sn} est une \textbf{primitive segment} où Armscribe écrit, \texttt{An} est une \textbf{primitive arc de cercle} où Armscribe écrit et \texttt{Dn} est une primitive où Armscribe se déplace pour former la lettre suivante.

Notre domaine d'opération $\mathcal{O}$ est un cercle de rayon $60$cm centré en l'origine du repère balisant l'espace opérationnel, ce cercle est visible en noir sur la figure \ref{fig:trajpolydes}. Sur la même figure, on peut remarquer en pointillés ronds les trajectoires planifiées pour les différents segments avec un taux d'échantillonnage assez bas. Ainsi, sur cette figure la densité des points décroît avec la vitesse, on peut constater le parcours curviligne paramétré par une vitesse delta-log-normale sur chacun des segments.
On peut voir également les segments de déplacement \texttt{Dn} affiché avec des \textit{dashlines} (--). 


\begin{figure}[h]
\centering
\includegraphics[width=0.62\textwidth]{fig/traj_des_poly.eps}
\caption{Trajectoire paramétrée numériquement dans Matlab.}
\label{fig:trajpolydes}
\end{figure}

Nous remarquerons que nos trajectoires sont analytiques en tout point de chacune des primitives, incluant les extrémités de ces dernières. Lors de la commutation entre deux primitives, les trajectoires demeurent lisses car $\bp_\mathrm{obj}(t_f^{i-1})^{i-1}=\bp_\mathrm{obj}(t_0^i)^{i}$ et les vitesses initiales et finales demeurent nulles. 

\subsection{Synthèse d'un contrôleur linéaire pour référence}

Afin de pouvoir comparer notre contrôleur non linéaire a une méthode linéaire, on synthétise un contrôleur PID axe par axe. Ce contrôleur fonctionne dans l'espace des articulations et nécessite donc l'utilisation de la cinématique inverse pour obtenir la trajectoire cible dans l'espace des articulation.
On synthétise la commande suivante en réglant les gains matriciels $\mbf K_P$, $\mbf K_D$, $\mbf K_I$ en utilisant \textit{l'approche douce} : 
$$ \bu =\mbf K_P (\bq_{\mathrm{obj}} - \bq) + \mbf K_D (\dot{\bq}_{\mathrm{obj}} - \dot{\bq}) + \mbf K_I \int (\bq_{\mathrm{obj}} - \bq) dt, $$
où $\bq_\mathrm{obj}$ est la commande désirée.

L'\textit{approche douce} multivariable ici n'est qu'une méthode de réglage \textit{ad-hoc} itérative, supposons les matrices $K_.$ diagonales :
\begin{itemize}
\item On règle le gain proportionnel $\mbf K_P$ pour obtenir une erreur relativement réduite en régime permanent sur une trajectoire simple (ex. ligne). Toutefois, il faut regarder l'effort induit et veiller à ce que $||\bu||$ ne soit pas trop élevée afin que la commande soit réalisable par des actionneurs;
\item On règle le gain en dérivée $\mbf K_D$ de la même façon, en  tentant de réduire l'erreur $\dot{\mbf e} = \bdq_{\mathrm{obj}}-\bdq$ ;
\item Enfin, on augmente lentement (d'où la terminologie \textit{approche douce}) le gain intégral $\mbf K_I$ afin de ne pas provoquer de divergence de la commande jusqu'à ce que la convergence soit admissible. C'est à dire que l'erreur en régime permanent soit réduite en un intervalle de temps raisonnable.
\end{itemize}
Nous remarquons tout de suite que l'approche linéaire ici n'est pas fondamentalement convenable car la dynamique (\ref{eq:dynamique}) du système est non-linéaire et en plus de ceci connue exactement. Le cadre de travail de ce projet nous amène à négliger d'éventuelles perturbations dont serait victimes les actionneurs au sein de notre asservissement. En effet, si on considère un robot \textit{armscribe} en train d'écrire en intérieur, nous pouvons nous affranchir desdites perturbations. En somme, nous connaissons parfaitement le modèle et sa dynamique est commandable (système totalement actionné); il est clair qu'une technique \textbf{d'inversion de dynamique} s'impose à nous.

\subsection{Synthèse d'une commande par couple pré-calculé}
Nous procédons procéder ici à \textbf{l'inversion de dynamique} afin de faire en sorte que le système ait un problème de commande formulé dans les \emph{coordonnées linéarisantes}.
\label{ss:controleurs}
\subsubsection{Synthèse dans l'espace articulaire}
Afin de compenser la dynamique non linéaire du système, utilisons son modèle dynamique (\ref{eq:dynamique}). 
La véritable difficulté de cette méthode est l'établissement d'un modèle dynamique de qualité et d'en prouver la commandabilité. Nous appuyant des discussions évoquées aux sections \ref{s:modelisation} (modélisation) et \ref{s:analyse} (analyse du système), nous pouvons réaliser cet asservissement.

La commande en couple (\textit{torque}) $\bu$ linéarisante s'exprime comme suit :
\begin{equation}
 \bu = \pmb \tau = \mbf H(\bq) \mbf v +  \mbf c(\bq,\dot{\bq}),
\end{equation}
où la \emph{consigne}, appelée également \emph{commande dans les coordonnées linéarisantes}, est $\mbf v$. Cette approche convertit notre problème de contrôle non linéaire en \textit{n = 2} problèmes linéaires découplées.
On choisit alors la consigne comme telle pour garantir les performances en suivi et en régulation :
\begin{equation}
 \mbf v = \bddq_{\mathrm{obj}} + \mbf K_P (\bq_{\mathrm{obj}} - \bq) + \mbf K_V (\bdq_{\mathrm{obj}} - \bdq) + \mbf K_I \int (\bq_{\mathrm{obj} } - \bq) \text{d}t,
\label{eq:loicdeart}
\end{equation}

la dynamique de l'erreur de suivi articulaires $\mbf e_q = \bq_{\mathrm{obj}} - \bq$ au sein de la loi (\ref{eq:loicdeart}) est : 
\begin{equation}
 \mbf e^{(3)}_{q} + \mbf K_V \ddot{ \mbf e}_{q} + \mbf K_P \ddot{ \mbf e}_{q} + \mbf K_I \mbf e_{q} = \mbf 0
 \label{eq:errdyn}
\end{equation}
La dynamique des erreurs est stable \cite{siciliano2016springer}  si les deux polynômes composant (\ref{eq:errdyn}) sont Hurwitziens. Notons que ce critère n'existe pas du tout pour la commande linéaire ce qui est une motivation pour choisir cette synthèse non-linéaire.

Afin de réaliser en pratique cet asservissement, nous devons obtenir des estimés des états articulaires fiables. Pour notre \textit{scenario} de simulation, nous supposons que les angles sont correctement mesurés par des encodeurs supposés idéaux. En ce qui concerne les vitesses angulaires, nous pouvons aisément trouver leur valeur en utilisant un filtre de Kalman \cite{chui_kalman_1999}, pour simplifier le cadre de nos simulations, nous supposons que les deux vitesses sont directement fournies.  

\subsubsection{Synthèse dans l'espace opérationnel}
Cependant, nous savons (voir section \ref{s:modelisation}) qu'il vaut mieux s'affranchir de la résolution du problème de cinématique inverse pour éviter les singularités cinématiques et la non unicité de la solution qui viendrait poser un problème de \textit{path planning}.

En utilisant les résultats établis en section (\ref{ss:modelisation}) pour l'espace opérationnel \cite{khatib_unified_1987}, on peut écrire à l'instar de (\ref{eq:loicdeart}) la loi de commande linéarisante :
\begin{equation}
\bu(t) = \pmb {\tau} = \bar{\mbf J}^{-T}(\mbf \Lambda(\bq) \mbf v +  \mbf b_t(\bq,\dot{\bq})),
\label{eq:loicdeop}
\end{equation}
avec une consigne similaire :
\begin{equation}
 \mbf v = \ddot{\mbf x}_{\mathrm{obj}} + \mbf K_P ({\mbf x}_{\mathrm{obj} } - {\mbf x}) + \mbf K_V (\dot{\mbf x}_{\mathrm{obj} } - \dot{\mbf x}) + \mbf K_I \int ({\mbf x}_{\mathrm{obj} } - {\mbf x}) \text{d}t
\end{equation}

Pour déterminer les matrices de gains $\mbf K_P$, $\mbf K_D$, $\mbf K_I$ on peut appliquer les méthodes par \emph{approche douce} utilisées pour la synthèse de PID.
Pour des cas à dimensions plus élevée en termes d'actionneur on peut formuler un problème d'optimisation pour optimiser ces coefficients. Ces coefficients nous donnent les pôles de l'erreur $\mbf e_d = \mbf x_\mathrm{obj} - \mbf x$ désirés et de leur impact sur l'amplitude de la commande linéarisante produite ainsi que leur robustesse. Notons que vient s'ajouter la contrainte Hurwitzienne afin d'obtenir un système stable en boucle fermée.




