
\clearpage
\section{Analyse du système}
\label{s:analyse}
\label{ss:analyse}


\subsection{Propriétés cinématiques du système}
\label{ss:propcin}
\subsubsection{Domaine d'opération du bras}

Le domaine d'opération $\mathcal{O}$ d'un bras est donné par la relation suivante :
\[ 
\bp \in \mathcal{O}, p_i \leq \kappa_i, \text{ où } \kappa_i = \underset{\bq}{\text{argmax}}~p_i(\bq)
\]
cette dernière peut se résoudre grâce à la cinématique directe du bras (\ref{eq:posoutil}). Dans notre cas, nous avons choisi $a_1 = a_2 = 0.3\mathrm{m}$, nous pouvons donc conclure que $\mathcal{O}=\mathcal{D}(0,a_1+a_2)=\mathcal{D}(0,0.6)$ où $\mathcal{D}(\mbf a, \rho)$ est le disque centré en $\mbf a$ et de rayon $\rho$. Nous devrons veiller à ce que notre \textbf{espace opérationnel} soit réduit à $\mathcal{O}$ pour nos trajectoires en position $\bp_{des}$ à poursuivre.

\subsubsection{Singularités cinématiques}

La définition de $\mathcal{O}$ nous donne grâce aux équations de cinématique directe (\ref{eq:posoutil}) et (\ref{eq:vitoutil}) une implication. Chaque position $\bx$ appartenant à $\mathcal{O}$ admet par définition une position articulaire $\bq$ correspondante. On peut raisonner de manière analogue pour $\bdx$ et $\bdq$. Ce passage entre l'espace articulaire $\mathcal{A}$ et l'espace opérationnel $\mathcal{O}$ ne présente aucune singularité car les fonctions sont continues.

En ce qui concerne la cinématique inverse ($\mathcal{O} \rightarrow \mathcal{A}$), ce n'est pas le cas à cause du problème de discontinuité des quadrants des angles. Si on regarde de plus près les équations de la section \ref{ss:cininv}, nous pouvons nous rendre compte du problème d'une façon plus directe: l'usage nécessaire de fonctions arctangentes discontinues en est la cause. En résulte une non-unicité des positions articulaires $\bq$ pour atteindre une même position opérationnelle $\bx$ \cite{corke_robotics_2011}.



En particulier si nous calculons la jacobienne géometrique inverse permettant le passage en vitesse entre les deux espaces :
\begin{equation}
\mbf J_{\mathcal{O} \rightarrow \mathcal{A}} 
:= 
\frac{\partial \mbf q}{\partial \mbf x} 
= 
\begin{bmatrix}
	\frac{s_{12}}{a_1c_{12}s_1 - a_1s_{12}c_1} & -\frac{c_{12}}{a_1c_{12}s_1 - a1s_{12}c_1} \\
	-\frac{a_2s_{12} + a_1s_1}{a_1a_2c_{12}s_1 - a_1a_2s_{12}c_1} & \frac{a_2c_{12} + a_1c_1}{a_1a_2c_{12}s_1 -a_1a_2s_{12}c_1}
\end{bmatrix}
\end{equation}


tel que $\bdq = \mbf J_{\mathcal{O} \rightarrow \mathcal{A}} \bdx$. On remarque que la quantité présente présente un déterminant nul pour $q_2 = \pi \text{ ou } -\pi $ ce qui donne un point singulier.


\subsection{Équations d'état du système}


\subsubsection{Commande dans l'espace articulaire}
Nous aurons besoin du vecteur des angles des joints $\bq$ et de la vitesse angulaire $\bdq$ résoudre notre problème de commande dans l'espace articulaire. En foi de quoi, notre vecteur d'état $\bx$ sera le suivant :
\[ \bx (t) := \begin{bmatrix}
\bq(t) & \bdq(t)
\end{bmatrix}^\top,
\]
de plus \textbf{la commande} $\bu(t)$ de notre système est formée des couples (\textit{torques}) des deux actionneurs :
\[ \bu(t) = \pmb \tau(t) = \begin{bmatrix}
\tau_1(t) & \tau_2(t)
\end{bmatrix}^\top,
\]
la sortie $\by(t)$ du système est la position $\bp(t)$ de l'outil, donc $\by(t) := \mbf h(\bx) = \bp(t)$ car on souhaite faire écrire Armscribe en contrôlant la position de ce dernier. Notons que $\mbf h(\bx)$ est définie par (\ref{eq:posoutil}).
ceci mène à l'équation totale du système grâce à (\ref{eq:dynamique}) :
\begin{equation}
\bdx := \mbf f(\bx) + \mbf g(\bu) 
=
\begin{bmatrix}
\bdq \\
-\mbf H^{-1}(\bq)\mbf c(\bq,\bdq)
\end{bmatrix} 
+
\begin{bmatrix}
\mbf 0 \\
\mbf H^{-1}(\bq) \mbf u
\end{bmatrix}.
\label{eq:modele_etat}
\end{equation}

Une commande par couple pré-calculé dans l'espace des articulations nécessite une transformation de la consigne dans l'espace opérationnel en utilisant la cinématique inverse et la cinématique instantanée inverse. Un passage par le point singulier produirai donc une erreur lors de l'utilisation de la jacobienne inverse $\mbf J_{\mathcal{O} \rightarrow \mathcal{A}}$. Pour traiter ce cas particulier, on calculera la jacobienne inverse que si le determinant de la jacobienne est suffisement grand. Cette astuce évite un crash du système mais produit de faibles performances en suivi lorsaue la trajectoire passe proche de 0, comme nous le verrons dans la section validation \ref{ss:validation}.
 
\subsubsection{Commande dans l'espace opérationnel}

Afin d'éviter des ambiguïtés trigonométriques dues à la cinématique inverse de notre système (discussions de la section \ref{ss:propcin}), notre approche est de raisonner sur un problème de régulation dans l'espace opérationnel. 
En foi de quoi, le vecteur d'état dans ce cas s'écrit :
\[
\bz = 
\begin{bmatrix}
\bp & \bdp
\end{bmatrix}^\top,
\]
Il est donc nécessaire de dévelloper le modèle dynamique du robot dans l'espace opérationnel, on se base sur le formalisme de Khatib \cite{khatib_unified_1987} : 

\begin{equation}
\bar{\mbf J}^{T} \pmb \tau = \mbf \Lambda(\bq) \ddot{\bp} +  \mbf b_t(\bq,\dot{\bq}) .
\label{eq:dynamique_ops}
\end{equation}
avec :
\begin{description}
\item [$ \mbf \Lambda(\bq) = (\mbf J^{T}_{\mathcal{A}\rightarrow \mathcal{O}} \mbf H^{-1} \mbf J_{\mathcal{A}\rightarrow \mathcal{O}})^{-1} $ :] Matrice de masse dans l'espace opérationnel; 
\item [$ \mbf b_t(\bq,\bdq) = \mbf{\Lambda}(\mbf {J}_{\mathcal{A}\rightarrow \mathcal{O}} \mbf{H}^{-1} \mbf{ c } - \dot {\mbf  J}_{\mathcal{A}\rightarrow \mathcal{O}} \dot {\mbf q}) $ :] Les forces centrifuges, de coriolis et gravitationnelles dans l'espace opérationnel; 
\item [$ \bar{\mbf J}^{T} = \mbf H^{-1} {\mbf  J}_{\mathcal{A}\rightarrow \mathcal{O}} \mbf \Lambda$ :] L'inverse généralisé de la jacobienne  ${\mbf  J}_{\mathcal{A}\rightarrow \mathcal{O}}$ qui minimise l'énergie cinétique du système.
\end{description}

le modèle d'état dans l'espace opérationnel s'écrit :
\begin{equation}
\bdz :=  \mbf f_o(\bz) + \mbf g_o(\bu) 
=
\begin{bmatrix}
\bdp \\
-\mbf \Lambda^{-1}(\bq) \mbf b_t(\bq,\bdq)
\end{bmatrix} 
+
\begin{bmatrix}
\mbf 0 \\
\mbf \Lambda^{-1}(\bq) \mbf u
\end{bmatrix}.
\label{eq:modele_etat_operationnel}
\end{equation}

et la sortie est simplement $\by = \mbf h_o(\bz) = \mbf z(1:2)$. 
On remarque qu'avec ce paradigme, l'inverse de la jacobienne ${\mbf  J}_{\mathcal{A}\rightarrow \mathcal{O}}$ n'est jamais utilisé.

\subsection{Propriétés dynamiques du système}
\label{ss:propdyn}
\subsubsection{Points d'équilibre accessibles}
 
Un point d'équilibre existe si et seulement si $\bdx = \mbf 0$. La première ligne de l'équation (\ref{eq:modele_etat}) nous donne immédiatement $\bdp = 0$ et la seconde :
\[ \mbf H^{-1}(\mbf q) [ \mbf u - \mbf c (\bq,\mbf 0)] =\mbf 0 \]
nous remarquerons que dans notre cas $\mbf H$ est définie positive (et donc inversible) \cite{corke_robotics_2011} car il s'agit d'une matrice d'inertie . Par conséquent, nous devons résoudre l'équation suivante pour trouver les points d'équilibre en utilisant (\ref{eq:dynamique}) et (\ref{eq:detailHC})  :
\begin{align}
\mbf u &= \mbf c (\bq,\mbf 0) \nonumber \\
\begin{bmatrix}
\tau_1 \\
\tau_2 
\end{bmatrix}
&=\begin{bmatrix}
-\frac{a_2gm_2}{2}s_{12} - g a_1(\frac{m_1}{2} + m_2) s_1 \\
-\frac{a_2}{2}g m_2 s_{12} 
\end{bmatrix}
\label{eq:equilibre}
\end{align}


Pour toute position dans l'espace des articulations $(q_1,q_2)$, on peut donc trouver un point d'équilibre si le moment engendré par les moteurs du bras articulé est suffisant. L'équation (\ref{eq:equilibre}) est en effet une \textbf{équation statique} de couples (bras de levier) le couple \footnote{Appelé également «moment» (\textit{momentum}) dans la littérature mécanicienne.} généré par les actionneurs doit être égal au couple engendré par la gravité. 

Le couple maximum pour atteindre l'équilibre est induit lorsque le bras est à l'horizontale $q_1 = q_2 = 0~[\pi]$ et respectivement le couple minimum l'est lorsqu'il est à la verticale $q_1 = q_2 = \pi/2~[\pi]$ ce qui est logique pour une structure rappelant celle du pendule inversé. 

%\td{condition d'accessibilité}

\subsubsection{Analyse qualitative de stabilité statique}
Nous allons vérifier ici la similarité entre notre système et celui du pendule inversé non commandé.


Si $\bu = 0$ alors on peut trouver que (\ref{eq:equilibre}) n'admet des solutions que si $q_1 + q_2 = 0~[\pi]$ (deuxième ligne) et $q_1=0~[\pi]$ (première ligne). 

\textbf{Cas 1 :} pour $q_1 = 0$, nécessairement $q_2 =0$ à l'équilibre donc $\bx^* = \mbf 0$.
On peut évaluer le linéarisé tangent de (\ref{eq:modele_etat}) numérique à ce point d'équilibre, on trouve sa valeur et son spectre $\sigma$ :
\[
Df_{|\bx = \bx^*} = 
\begin{bmatrix}
         0     &    0  &  1.0000  &       0 \\
         0     &    0  &       0  &  1.0000 \\
   42.0183 & -41.9950  & -6.3459  & 15.8622 \\
  -56.0088 & 153.9893  & 15.8622 & -50.7569 
\end{bmatrix}
,
\sigma(Df_{|\bx = \bx^*}) = 
\left\{
  -58.7680,
   -5.5660,
    4.3101,
    2.9211
\right\}
.
\] 
comme pour le pendule inversé, le point vertical haut est un point d'équilibre instable (le linéarisé tangent admet des valeurs propres à partie réelle positive).
 
\textbf{Cas 2 :} pour $q_1 = \pi$, nécessairement $q_2 =0$ à l'équilibre donc $\bx^* = \begin{bmatrix}
\pi & 0 & 0 & 0
\end{bmatrix}^\top$
et donc : 
\[
Df_{|\bx = \bx^*} = 
\begin{bmatrix}
        0  &        0  &  1.0000 &        0 \\
         0 &        0  &       0 &   1.0000 \\
  -42.0183 &  41.9950  & -6.3459 &  15.8622 \\
   56.0088 & -153.9893  &  15.8622 & -50.7569
\end{bmatrix}
,
\begin{array}{c}
\sigma(Df_{|\bx = \bx^*}) =
\{ -52.5677,
  -0.6327 + 4.8538i, \\
  -0.6327 - 4.8538i,
  -3.2697  \}
\end{array}.
\] 
logiquement dans la position verticale basse, le point d'équilibre trouvé est stable (le linéarisé tangent a toutes ses valeurs propres à partie réelles négatives).
 \subsubsection{Commandabilité}

La question est ici de savoir si tous les états souhaités du système sont atteignables dans un temps fini en appliquant une commande $\bu$ finie \textit{ie} de savoir si on peut concevoir un contrôleur pour suivre des primitives cinématiques. Cela revient à prouver tout couple est atteignable $\{ \bp, \bdp \} \in \mathcal{O} \times \mathbb{R}^2$ ce qui est équivalent à prouver dans l'espace articulaire que tout $\{ \bq, \bdq \} \in \mathcal{A} \times \mathbb{R}^2$ est atteignable.

Dans cette partie nous raisonnerons donc sur l'espace des articulations $\mathcal{A}$ et donc sur le vecteur $\mbf x$. Cette approche nous affranchit de calculs supplémentaires non-nécessaires. En effet, les considérations faites à la section \ref{ss:propcin} nous amène au constat suivant : si on peut commander le système dans tout état articulaire dans $\mathcal{A}$ alors on peut le commander dans tout l'espace articulaire $\mathcal{O}$ (respectivement pour les états des vitesses).

Tout d'abord, on va noter 
$ \mbf H^{-1}= \begin{bmatrix}
\mbf h_1 & \mbf h_2
\end{bmatrix}$ 
l'équation (\ref{eq:dynamique}) nous donne les vecteurs de la base de la commandabilité :
\[
\mbf f_0 = 
\begin{bmatrix}
\bdq \\
-\mbf H^{-1} \mbf C
\end{bmatrix},
\quad
\mbf f_1 = 
\begin{bmatrix}
\mbf 0_{21} \\
\mbf h_1
\end{bmatrix},
\quad
\mbf f_2 = 
\begin{bmatrix}
\mbf 0_{21} \\
\mbf h_2
\end{bmatrix},
\] 
nous pouvons donc calculer :
\begin{align*}
\mbf f_3 &= [\mbf f_1, \mbf f_0] = \frac{\partial \mbf f_0}{\partial \bx} \mbf f_1 -  \frac{\partial \mbf f_1}{\partial \bx} \mbf f_0 \\
&= 
\begin{bmatrix}
\mbf 0_2  & \mbf I_2 \\
\mbf A_{21} & \mbf A_{22}
\end{bmatrix}
\begin{bmatrix}
\mbf 0_{21} \\
\mbf h_1
\end{bmatrix}
-
\begin{bmatrix}
\mbf 0_2  & \mbf 0_2 \\
\frac{\partial h_1}{\partial \bx} & \mbf 0_2
\end{bmatrix}
\begin{bmatrix}
\bdq \\
-\mbf H^{-1} \mbf c
\end{bmatrix}
=
\begin{bmatrix}
\mbf h_1 \\
\mbf A_{22} \mbf h_1 + \frac{\partial h_1}{\partial \bx} \mbf H^{-1} \mbf c 
\end{bmatrix}
:=
\begin{bmatrix}
\mbf h_1 \\
\mbf b_1
\end{bmatrix}
\end{align*}
avec $\mbf b_1 , \mbf A_{21} \mbf A_{22}$ des variables auxiliaires. De manière analogue, construisons :
\[ 
\mbf f_4 = [\mbf f_2, \mbf f_0] = 
\begin{bmatrix}
\mbf h_2 \\
\mbf b_2
\end{bmatrix}, 
\]
il faut se souvenir que $\mbf H>0$, et est donc de rang plein $\text{rank}(\mbf H) = 2$, en concaténant les vecteurs, et en posant $\mbf B = \begin{bmatrix}
\mbf b_1 & \mbf b_2
\end{bmatrix}$ on obtient :
\[
\dim (\text{span}\left\{ \mbf f_1 , \mbf f_2, \mbf f_3, \mbf f_4 \right\})
=
\dim \mbf W :=
\dim \left(
\begin{bmatrix}
\mbf 0_2 & \mbf H^{-1} \\
\mbf H^{-1} & \mbf B
\end{bmatrix}
\right),
\]
sachant que le rang de $\mbf H$ est plein, $\mbf H^{-1}$ l'est aussi. Les deux premières colonnes de $\mbf W$ sont indépendantes entre elles
comme les deux dernières à cause de la présence de $\mbf H^{-1}$ \footnote{Vérifiant $\det \mbf H^{-1} \neq 0$.}. Les deux ensembles de colonnes sont linéairement indépendant car les deux premières colonnes ont leurs deux premières lignes nulles. On peut donc conclure que $\mbf W$ est de rang plein quel que soit $\mbf x \in \mathbb{R}^4$. Le système est \textbf{complètement commandable} ce qui est logique puisque chaque joint est rigide et comporte son propre actionneur.
%\subsection{Poursuite de trajectoires lisses}
%
%\td{passera en partie contrôle peut être}

