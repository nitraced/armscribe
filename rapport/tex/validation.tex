\section{Validation par simulations}
\label{ss:validation}

Nous utilisons pour ce faire la suite logicielle Matlab-Simulink qui inclut les paramètres avec les paramètres expliqués à la section \ref{s:modelisation}.

\subsection{Espace des articulations \textit{versus} espace opérationnel}
Confirmons l'intérêt d'utiliser un modèle dynamique dans l'espace opérationnel. Nous créons une une trajectoire rectiligne passant par le point singulier $ \mbf 0 $ de la cinématique inverse. Cette consigne est fournit comme telle au contrôleur par couple pré-calculé dans l'espace opérationnel. Et elle est transformée dans l'espace des articulations par l'application de la cinématique inverse avant d'être envoyée au contrôleur par couple pré-calculé dans l'espace des articulations. 

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{fig/validation/joint_space/with_singularity/xz_merge.eps}
\caption{Suivi d'une trajectoire passant par zéro en utilisant un modèle dans l'espace des articulations et un modèle dans l'espace .}
\label{fig:singularity}
\end{figure}

Le contrôleur dans l'espace opérationnel (\textit{ops space} sur la figure) suit parfaitement la trajectoire et gère la singularité cinématique. Ceci grâce à l'utilisation de l'inverse généralisé de la jacobienne pour construire le modèle dynamique dans l'espace opérationnel. 
La cinématique inverse instantanée nécessaire au contrôleur dans l'espace des articulations (\textit{joint space} sur la figure) pour déterminer la consigne en vitesse ne peut pas gérer la position $ \mbf 0 $. 

A cet endroit, la jacobienne est de déterminant nul. Pour contourner le problème on attribue la valeur arbitraire $0$ à l'inverse de la jacobienne lorsque son déterminant passe en dessous d'un seuil critique. Cette solution peu élégante permet au système de retrouver la trajectoire après la singularité mais donne de mauvaises performances au passage de la singularité. En effet, nous perdons momentanément la continuité du signal de référence. Nous utiliserons pour la suite de la validation le \textbf{contrôleur par couple pré-calculé dans l'espace opérationnel}, solution qui nous évite le problème de singularité cinématique avec aisance.

\subsection{Étude comparative des contrôleurs linéaire et non linéaires}
On analyse les performances du contrôleur \emph{linéaire} \textbf{PID} et du contrôleur \emph{non-linéaire} par \textbf{couple pré-calculé} avec une consigne en position et en vitesse delta-lognormale, nous obtenons les résultats présentés aux figures \ref{fig:poly_position} et \ref{fig:poly_errors}.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{fig/validation/poly/xz_merge.eps}
\caption{Suivi d'une trajectoire complexe avec un PID et avec un couple pré-calculé}
\label{fig:poly_position}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{fig/validation/poly/errors_merge.eps}
\caption{Erreurs de suivi avec un PID et avec un couple pré-calculé}
\label{fig:poly_errors}
\end{figure}
La figure \ref{fig:poly_errors} révèle des performances de $30$ à $50\%$ meilleures en suivi pour le contrôleur par couple pré-calculé (\textit{fb lin} dans les légendes). Et ceux, pour des commandes quasi-identiques en terme de normes et donc en terme d'énergie dépensée. 

 On remarque que le PID est efficace en \textbf{maintien de position} aux points d'arrêts mais produit de faible performances lors des mouvements dynamiques entre les points d'arrêts. On peut remarquer ce caractère à la figure \ref{fig:poly_errors} : des oscillations apparaissent pour le tracé de $||\bu(t)||$,$\bx$ et $\bdx$ car le système n'intègre pas le modèle dynamique non-linéaire mais cherche à converger en raison de son action intégrale (réglage par \emph{approche douce}).  
 
  Comme notre cas d'étude met l'accent sur l'importance de la consigne en vitesse delta-lognormale pour reproduire les gestes humains, le contrôleur par couple pré-calculé se présente comme la solution évidente, malgré que sa synthèse prenne plus de temps. Notamment en raison de  la nécessité d'établir un modèle mécanique le plus exact qui soit, ce qui est nécessaire pour procéder à une \textbf{inversion dynamique}.
  
 

\FloatBarrier
\subsection{Étude de robustesse paramétrique}
 \label{ss:robustesse}
 
 L'efficacité d'un contrôleur par couple pré-calculé est basée sur la précision du modèle. Si on estime les paramètres de notre modèle de manière erratique, on peut inclure dans la commande des éléments de \emph{commande adaptative}. Par contre, si le système est soumis à de fortes perturbations perturbations, la théorie de la \emph{commande robuste} s'impose. Toutefois, le \textit{scénario} d'utilisation d'\textit{Armscibe} est dans un environnement contrôlé sans trop de perturbation, le \textbf{problème d'adaptation} des gains en fonction d'une \textbf{incertitude paramétrique} nous semble plus judicieux à étudier.
 

Afin de garantir une implémentation adéquate, il est nécessaire que notre synthèse soit robuste aux mauvaises estimations paramétriques. En effet, les inerties sont difficilement estimables et il est inadmissible qu'un opérateur d'\textit{Armscribe} ait à calibrer ce dernier à chacune de ses utilisations. Nous allons voir comment les erreurs d'estimation de ces matrices influent dans la dynamique bouclée du système avec le \textbf{compensateur à couple pré-calculé dans l'espace opérationnel}.
 


 \begin{table}[h]
  \caption{Gains utilisés par le contrôleur à couple pré-calculé dans l'espace opérationnel.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline 
  Gains & $\mbf K_P$ & $\mbf K_D$ & $\mbf K_I$ \\ 
  \hline 
  Sans augmentation & 20$\mbf I_2$ & 12$\mbf I_2$ & 0.3$\mbf I_2$ \\ 
  \hline 
  Augmentés & 50$\mbf I_2$ & 30$\mbf I_2$ & $\mbf I_2$ \\ 
  \hline 
  \end{tabular} 
  \label{tab:gains}
  \end{table}
  
  
  L'erreur d'estimation paramétrique $\epsilon$, pour l'estimé $\tilde{ J}_{iZ}$ du moment d'inertie $J_{iZ}$ est définie ainsi : 
  \[
  \tilde{ J}_{iZ} = (1-\epsilon) J_{iZ},
  \] 

dans notre cas, nous étudions le suivi de trajectoire avec un contrôleur utilisant un modèle estimé avec une erreur de 5\% puis avec une erreur de 50\%. En accord avec notre définition de l'erreur, notre synthèse \textbf{sous-estime} les inerties du système réel. 


Nous testons d'abord avec les gains initiaux utilisés pour la validation sous bonne condition (test de la figure \ref{fig:poly_position}), puis nous augmentons drastiquement les gains pour essayer de retrouver de meilleures performances. La table \ref{tab:gains} montre les différents réglages entrepris pour ce faire.

\subsubsection{Erreur d'estimation de 5\%}
On trace sur les figures \ref{fig:est5_pos} et \ref{fig:est5_com} la position et l'effort de commande avant et après l'augmentation des gains.
\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{fig/validation/poly_estimated/xz5_merge.eps}
\caption{Suivi d'une trajectoire complexe un couple pré-calculé basé sur un modèle erroné de 5\% avant et après une augmentation drastique des gains}
\label{fig:est5_pos}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{fig/validation/poly_estimated/errors5_merge.eps}
\caption{Commande avant et après une augmentation drastique des gains}
\label{fig:est5_com}
\end{figure}

Nous remarquons que la sous-évaluation du système induit une commande sous-dimensionnée visible à la figure \ref{fig:est5_com}, l'augmentation des gains nous permet de mieux compenser l'écart induit par l'erreur d'estimation. Toutefois, même sans ajout de gain, on peut remarquer que les point finals des primitives décrites par le bras (figure \ref{fig:est5_pos}) sont respectés. Cela est dû au caractère log-normal de la trajectoire tendant à avoir une vitesse nulle $v(t_f^i)=0$ en toute fin de segment $(i)$, ceci permet à la composante intégrale de notre consigne dans les coordonnées linéarisée de rattraper cette dernière. 

En fait, la défaillance d'estimation fait en sorte que $\bu$ ne soit plus exactement linéarisante : ainsi le comportement du contrôleur pour $\mbf v$ est celui d'un PID dans des coordonnées non-linéaires. Sa composante intégrale finira par annuler l'erreur et ses gains doivent être augmentés pour assurer une annulation. Chose que nous pouvons voir sur la figure \ref{fig:est5_pos} la trajectoire $\bp(t)$ (\textit{increased gains}) semble se rapprocher davantage de celle de $\mbf p_\mathrm{obj}$ (\textit{des}).



\subsubsection{Erreur d'estimation de 50\%}
Pour $\epsilon=0.5$ soit une sous-estimation de 50\% nous procédons au même test avec les même gains du tableau \ref{tab:gains}.

On trace sur les figures \ref{fig:est50_pos} et \ref{fig:est50_com} la position et l'effort de commande avant et après l'augmentation des gains.

On remarque que l'augmentation drastique des gains mène à une faible augmentation des efforts de commande et logiquement à une amélioration des performances. 
Dans le cas du modèle estimé à 50\%, les performances en termes de suivi sont étonnamment convenables pour une si grande erreur et l'augmentation de l'effort de commande reste acceptable (environ 5\%). 

Pour obtenir de meilleures performances, on pourrait se tourner vers la \emph{commande adaptative} afin d'estimer les paramètres inconnus de notre modèle. Ceci prendrait d'étudier avec plus de rigueur la stabilité de notre système qui deviendrait ainsi augmenté (dynamique de l'observateur) nous pourrions alors utiliser une approche par \textit{backstepping} pour convenablement concevoir notre commande adaptative.

Cependant, le test que nous avons mené ici est très pessimiste au vu de l'erreur d'estimation et montre que même sans connaître très précisément les paramètres du modèle nous sommes capables de l'asservir d'une manière adéquate. Ceci prouve que notre système est implémentable en l'état. 

\FloatBarrier
\begin{figure}[p]
\centering
\includegraphics[width=0.7\textwidth]{fig/validation/poly_estimated/xz50_merge.eps}
\caption{Suivi d'une trajectoire complexe un couple pré-calculé basé sur un modèle erroné de 50\% avant et après une augmentation drastique des gains}
\label{fig:est50_pos}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=0.7\textwidth]{fig/validation/poly_estimated/errors50_merge.eps}
\caption{Commande avant et après une augmentation drastique des gains}
\label{fig:est50_com}
\end{figure}



%\FloatBarrier

\clearpage
\section{Conclusion}

La mise en équations de notre problème fut sans doute le plus grand défi que nous eûmes à relever au sein de ce projet. La modélisation utilise des outils de dynamique avancés comme les méthodes de Denavit Hartemberg ou encore des transformations telles que les tranformées de Plücker nous permettant la résolution des problèmes dans les deux systèmes de coordonnées précédemment évoquées. Une fois cette modélisation faite, notre tâche fut de développer une suite de test, incluant la planification de trajectoire, la dynamique du système, la synthèse de contrôleur, leur implémentations et finalement un banc de validation.


Nous avons pu appliquer les notions du cours \emph{ELE6204A} afin de mener à bien ce projet : nous avons effectué une analyse de ce système afin de justifier par la suite une synthèse de contrôleur permettant d'effectuer une poursuite de signaux non-linéaires avec une dynamique également non-linéaire. Nous avons pu tirer avantage de la synthèse en inversion dynamique afin de procurer des performances adéquates à \textit{Armscribe}. On remarquera que cette dernière est robuste bien qu'améliorables, notamment par des outils non-linéaires tels que la commande adaptative. Ceci étant une avenue demeurant à explorer par la suite.


Notre contribution vise surtout à l'implémentation de lois de contrôles dans l'espace opérationnel permettant s'affranchir de singularités cinématiques. Ceci fut effectivement un défi relevé, permettant la poursuite de trajectoires delta-lognormales de manière aisée en effectuant un nombre minimal de calibration (seuls les gains de la commande PID pour $\mbf v$). 

Naturellement, étant tous deux des roboticiens, nous pensons à l'implémentation physique d'un tel système sur un bras manipulateur. L'équipe \textit{Scribens} du département de Génie Électrique serait intéressée par le déploiement de cet asservissement. En effet, le déploiement du système sur un bras robotique, permettrait en outre de valider des identifications de paramètres log-normaux prises sur des sujets humains en reproduisant leur écriture sur une tablette. 




