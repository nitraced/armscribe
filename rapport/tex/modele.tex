\section{Modélisation du système}
\label{s:modelisation}
\label{ss:modelisation}

\subsection{Avant-propos}

\subsubsection{Simplification du modèle du bras complet}

%\td{Rédiger explication de la cinématique.}

Le bras Jaco est un bras articulé à six degrés de liberté. Afin de mener l'étude dynamique du système, nous allons contraindre le bras à deux degrés seulement car notre but est de faire en sorte qu'\textbf{armscribe} écrive sur une surface plane. 

Notre dynamique sera celle d'un robot RR placé dans un plan tangentiel au vecteur gravité: la situation étudiée est une écriture sur un \textbf{tableau vertical}. Un robot RR signifie robot Rotoïde-Rotoïde, c'est-à-dire doté de deux liaisons pivot actionnant deux bielles. Une des liaisons pivot (1) est supposée être liée au référentiel immobile du bâti $\mathcal{R}_0$. Cette dernière actionne la bielle de longueur $a_1$ au bout de laquelle se trouve la liaison pivot (2) actionnant à son tour une bielle de longueur $a_2$ au bout de laquelle se trouve le stylo dessinant les graphèmes. Le système est résumé à la figure \ref{fig:modele_cinematique_simplifie}.  

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{fig/cinematique.pdf}
\caption{Modèle cinématique simplifié du bras robotique.}
\label{fig:modele_cinematique_simplifie}
\end{figure}

\subsubsection{Notations}




\begin{description}
\item [Angles :] Les repères associés au joint $i = \{1,2\}$ seront notés $\mathcal{R}_i$ le repère $\mathcal{R}_0$ est le repère de la base qui servira de \textbf{référentiel absolu}.
\item [Angles :] Nous noterons les angles $\mbf q(t) = \begin{bmatrix} q_1(t) & q_2(t) \end{bmatrix}^T$ des joints rotoïde du bras. Dans nos discussions nous omettrons, afin d'alléger la notation, qu'ils sont fonction du temps.
\item [Outil :] Les coordonnées du point $M(t)$ où se situe l'outil (le crayon) dans l'espace de travail seront notés $x(t),z(t)$.
\item [Fonctions trigonométriques :] on introduit les notations $c_i = \cos(q_i)$ et $s_i = \mathrm{s}(q_i)$. On introduit aussi les sommes d'angles $q_{ij}=q_i + q_j = q_{ji}$ et leurs opérateurs respectifs $c_{ij}=\cos(q_{ij})$ et $s_{ij}=\mathrm{s}(q_{ij})$.   
\end{description}


\subsubsection{Paramètres de Denavit-Hartemberg}

Le robot RR possède des paramètres reportés dans la table \ref{tab:dh} de Denavit-Hartemberg. 

 
\begin{table}[h]
\caption{Table de Denavit-Hartemberg}
\centering
\begin{tabular}{c|c|c|c|c|}
 Joint& $\alpha$  & $\theta$  & $a$ & $d$  \\
 \hline
 1 	  & 0 & 0 & $a_{1}$ & 0 \\
 2    & 0 & 0 & $a_{2}$ & 0 
\end{tabular}
\label{tab:dh}
\end{table}

Ces paramètres sont sont une norme standardisée pour décrire les robots manipulateurs \cite{corke_robotics_2011}: ils représentent les angles de décalage et les translations entre deux liaisons pivot d'angle variable $q_i$ lorsque $q_i=0$ comme l'indique la figure \ref{fig:dh_schema}.


\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{fig/dh_parameters.png}
\caption[Paramètres de Denavit-Hatenberg]{Paramètres de Denavit-Hatenberg (tiré de \cite{dh})}
\label{fig:dh_schema}
\end{figure}

\subsection{Cinématique directe}
\label{ss:cindir}

Nous exprimons $M(t)$ dans $\mathcal{R}_0$ en fonction des angles des joins rotoïdes, ainsi, nous obtenons :
\begin{equation}
M(\mbf q) := \mbf p(\mbf q) = 
\begin{bmatrix}
x \\
z 
\end{bmatrix}
=
\begin{bmatrix}
-a_1 s_1 - a_2 s_{12} \\
a_1 c_1 + a_2 c_{12} 
\end{bmatrix},
\label{eq:posoutil}
\end{equation}
en dérivant (\ref{eq:posoutil}) on obtient la cinématique directe instantanée en utilisant la jacobienne géométrique $\mbf J_{\mathcal{A} \rightarrow \mathcal{O}}$ permettant de passer de l'espace opérationnel à l'espace des articulations. On obtient alors la vitesse du point $M(t)$ :
\begin{equation}
 \bdp(\mbf q,\bdq) = 
\begin{bmatrix}
\dot x \\
\dot z 
\end{bmatrix}
=
\mbf J_{\mathcal{A} \rightarrow \mathcal{O}} (\mbf q) \dot{\mbf q}
=
\begin{bmatrix}
-a_1 \dot{q}_1 c_1 - a_2 \dot{q}_{12} c_{12} \\
-a_1 \dot{q}_1 s_1 - a_2 \dot{q}_{12} s_{12} 
\end{bmatrix},
\label{eq:vitoutil}
\end{equation}
en dérivant (\ref{eq:vitoutil}) on obtient l'accélération du point $M(t)$ :

\begin{equation}
\mbf a(\mbf q) = 
\begin{bmatrix}
\ddot x \\
\ddot z 
\end{bmatrix}
=
\begin{bmatrix}
-a_1 \left[ \ddot{q}_1 c_1 - \dot{q}_1^2 s_1 \right] 
-a_2 \left[ \ddot{q}_{12} c_{12} - \dot{q}_{12}^2 s_{12} \right] \\
-a_1 \left[ \ddot{q}_1 s_1 + \dot{q}_1^2 c_1 \right] 
- a_2 \left[ \ddot{q}_{12} s_{12} + \dot{q}_{12}^2 c_{12} \right] 
\end{bmatrix}.
\label{eq:accoutil}
\end{equation}


\subsection{Cinématique inverse}
\label{ss:cininv}

On appelle cinématique inverse le passage de l'espace des coordonnées cartésiennes à l'espace articulaire. C'est à dire résoudre l'équation cinématique (\ref{eq:posoutil}) en se donnant la position finale $\bp$ de l'outil pour extrapoler le vecteur $\bq$.

Numériquement, en utilisant les paramètres présents dans le tableau
\ref{tab:simvalues}, on peut donner un exemple de résolution pour atteindre une position $\mbf p = \begin{bmatrix}
x & z
\end{bmatrix}^\top$ donnée :
\begin{equation}
\bq(\bp) = 
\left[\begin{array}{c} 2\,\text{atan2}\left(3\,x-\sqrt{-25\,x^4-50\,x^2\,z^2+9\,x^2-25\,z^4+9\,z^2},5\,x^2+5\,z^2+3\,z\right) \\ 2\,\mathrm{atan}\left(\frac{\sqrt{-25\,x^2-25\,z^2+9}}{5\,\sqrt{x^2+z^2}}\right) \end{array}\right]%\normalsize
\label{eq:exemplecininv}
\end{equation}


Le lecteur notera le problème de l'usage de cette formulation: une singularité apparaît lorsque $\mbf p =0$ ce qui est extrêmement gênant pour effectuer du contrôle directement dans l'espace articulaire.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{fig/cininv.pdf}
\caption{Non unicité de la cinématique inverse pour un robot RR.}
\label{fig:cininv}
\end{figure}

Nous noterons également que la résolution de la cinématique inverse peut mener à un calcul de plusieurs solutions admissibles ce qui complexifie la couche de planification de trajectoire. 

Nous pouvons noter ceci à la figure \ref{fig:cininv} où deux configuration en bleu et en rouge du même robot parviennent à générer la même position $\bp$ de l'outil.

L'usage de la cinématique inverse est populaire en robotique \cite{corke_robotics_2011} mais doit être utilisée de manière parcimonieuse en raison des singularité et du fait que sa solution n'est pas unique.

\subsection{Équations dynamiques}
\label{ss:dyna}
L'équation dynamique pour un bras manipulateur dans un champ de gravité uniforme $\mbf g = -g \mbf z_0$ est donnée par :

\begin{equation}
\mbf \tau = \mbf H(\bq) \ddot{\bq} +  \mbf c(\bq,\dot{\bq}) .
\label{eq:dynamique}
\end{equation}
avec :
\begin{description}
\item [$\mbf H$ :] Matrice de masse définie positive;
\item [$\mbf c$ :] vecteur $\mbf c(\bq,\dot{\bq})=\mbf h(\bq,\dot{\bq})+\mbf g(\bq)$  contenant des vecteurs des forces de Coriolis et centrifuges $\mbf h(\bq,\dot{\bq})$  ainsi que le terme de gravité $\mbf g(\bq)$;
\item [$\mbf \tau$:] Couple imposé par les moteurs des joints (1) et (2). 
\end{description}


Les termes de cette équation dynamique générale, donnée par un algorithme récursif pour une table standard de Denavit-Hartemberg peut se calculer aisément à partir de Jacobiennes succesives des états cinématiques des joints, du vecteur gravité dans chacun des repères et des paramètres inertiels des bielles \cite{corke_robotics_2011}. L'algorithme RNEA est à ce jour le plus efficace \cite{luh_-line_1980}. Nous avons implémenté cet algorithme sur \texttt{Matlab} pour la table \ref{tab:dh}, qui nous donne les deux matrices suivantes :

\begin{align}
\mbf H (\bq) &=
\left[\begin{array}{cc} m_{2}\,a_{1}^2+2\,m_{2}\,c_2\,a_{1}\,\frac{a_2}{2}+m_{1}\,{\frac{a_1}{2}}^2+m_{2}\,a_{2}^2+\mathrm{J1z}+\mathrm{J2z} & 
m_{2}\,a_{2}^2+a_{1}\,m_{2}\,c_2\,\frac{a_2}{2}+\mathrm{J2z}
\\ m_{2}\,a_{2}^2+a_{1}\,m_{2}\,c_2\,\frac{a_2}{2}+\mathrm{J2z} & 
m_{2}\,a_{2}^2+\mathrm{J2z} \end{array} \nonumber
\right],
 \\
\mbf c(\bq,\dot{\bq}) &=
\left[\begin{array}{c} -\,a_{1}\,\frac{a_2}{2}\,m_{2}\,s_2\,{\dot{q}_{2}}^2-2\,a_{1}\,\frac{a_2}{2}\,m_{2}\,\dot{q}_{1}\,s_2\,\dot{q}_{2}+\eta\,\dot{q}_{1}-\,\frac{a_2}{2}\,g\,m_{2}\,s_{12}-\,a_{1}\,g\,m_{2}\,s_1-\,\frac{a_1}{2}\,g\,m_{1}\,s_1
\\ a_{1}\,\frac{a_2}{2}\,m_{2}\,s_2\,{\dot{q}_{1}}^2+\eta\,\dot{q}_{2}-\,\frac{a_2}{2}\,g\,m_{2}\,s_{12} \end{array}\right], 
\label{eq:detailHC}
\end{align}

avec comme paramètres pour $i\in \{1,2\}$ :
\begin{description}
\item [$m_i$ :] les masses des bielles;
\item [$\mathrm{Jiz}$ :] les moments d'inertie projetés suivant les axes $\vec{z}_i$;
\item [$a_i$ :] les longueurs des bielles. \textbf{Note :} Les coefficients en $\frac{a_i}{2}$ représentent la distance entre les articulations et les centres de masses situés au centre de chacune des bielles (elles ont une densité linéique de masse supposément uniformes selon $\vec{z}_i$).
\item [$\eta$ :] coefficient de frottement visqueux des liaisons pivot (appelés également «joints rotoïde» par anglicisme dans la littérature francophone);
\item [$c_i \text{ respct. } s_i$ :] fonction trigonométrique $\cos(q_i)$ en fonction des angles des liaisons pivot $q_i$, respect. $\sin(q_i)$. 
\end{description}

\subsection{Simulateur et paramétrisation numérique du problème}

\subsubsection{Suite logicielle de simulation}

Nous avons développé un environnement de simulation compatible sur la suite logicielle \\ \texttt{Matlab-Simulink R2018a}. Notre approche fut de diviser en quatre grands axes notre code :
\begin{description}
\item [Générateur de trajectoire :] fournit des trajectoires delta-lognormales en vitesse curviligne pour des primitives de forme simple (circulaire et rondes), le contenu de ce bloc sera discuté à la section \ref{ss:motionplanner} du présent rapport.
\item [Modèle cinématique et modèle cinématique inverse :] Implémentation de ce qui fut décrit aux sections \ref{ss:cindir} et \ref{ss:cininv}.
\item [Modèle dynamique :] Implémentation de ce qui fut décrit à la section \ref{ss:dyna}.
\item [Contrôleur :] Implémentation des contrôleurs discutés à la section \ref{ss:controleurs}.
\end{description}

L'architecture des flux de données au sein du logiciel est résumée à la figure \ref{fig:dataflow}. Les blocs relatifs au contrôle et à la dynamique du système utilisent des fonctions de la bibliothèque de Peter Corke \texttt{Robotics Toolbox} \cite{toolbox}. Les paramètres numérique de notre modèle sont des paramètres standards pour un robot manipulateur léger. Ceci dans le sens où la charge utile de ce dernier, un stylo, est peu pesante. Les paramètres utilisés pour nos simulations sont consignés dans le tableau \ref{tab:simvalues}.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{fig/archilog.eps}
\caption{Flux de données au sein de nos simulateurs.}
\label{fig:dataflow}
\end{figure}

En ce qui concerne les valeurs numériques des paramètres, notons toutefois que nous pourrons ajouter des incertitudes afin de tester la \textbf{robustesse} de nos contrôleurs à une mauvaise estimation de ces derniers. Une discussion qui sera menée à la section \ref{ss:robustesse}.

\begin{table}[h]
\caption{Valeurs utilisées pour nos simulations}
\centering
\begin{tabular}{|c|c|c|c|}
\hline 
Description & Paramètre & Joint 1 & Joint 2 \\ 
\hline 
Masse & $m_i$ & 0.03 & 0.03 \\ 
\hline 
Moment d'Inertie & $Ji_z$ & 2.2525e-04 & 2.2525e-04 \\ 
\hline 
Longueur des bielles  & $a_i$ & 0.3 & 0.3 \\ 
\hline 
\dotfill & \dotfill & \dotfill & \dotfill \\ 
\hline 
Accélération de la pesanteur & $g$ & 9.8066 & idem \\ 
\hline 
Frottement visqueux & $\eta$ & 0.1 & idem \\ 
\hline 
\end{tabular} 
\label{tab:simvalues}
\end{table}

\subsubsection{Validation de la dynamique}

Nous avons validé notre simulateur de la dynamique du système en effectuant des trajectoires test sur l'outil de modélisation mécanique \texttt{Simscape}, appartenant à la suite logicielle \texttt{Matlab}. Ainsi, en spécifiant les mêmes trajectoires en boucle ouverte à notre système nous avons pu confronter les deux simulateurs (modèle analytique déployé \texttt{Simulink} et modèle dynamique par blocs sur \texttt{Simscape}) sur chacun des états.

Dans l'élaboration de notre banc de test numérique, ceci est extrêmement important car la probabilité d'une erreur en mécanique des solide est grande. En effet, lorsque plusieurs repères interviennent, il existe des représentations équivalentes dans chacun des repères à une transformation rigide près. Si cette dernière est faussée, le contrôle qui en découle l'est. 

Le banc de test sur \texttt{Simscape} est visible à la figure \ref{fig:simscape}. Nous pouvons y voir les transformations entre les repères, les efforts (couples de frottement et actionneurs) au sein des deux liaisons, la configuration du repère 0 (gravité, orientation absolue, méthodes de résolution numérique adoptée) ainsi que les exports des données pour le bloc \texttt{Simulink}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{fig/simscape.png}
\caption{Banc de test et modélisation \texttt{Simscape}.}
\label{fig:simscape}
\end{figure}

La première vérification de la paramétrisation de notre modèle sur \texttt{Simulink} a été la comparaison entre ses trajectoires et celles fournies par \texttt{Simscape} pour une chute libre (lâcher du bras à une condition initiale arbitraire sans tension de commande des actionneurs). Elle nous a permis de réaliser un premier déverminage de notre paramétrisation des repères et ainsi pouvoir effectuer par la suite une commande actionnée et \textit{in fine} l'asservissement du bras.
