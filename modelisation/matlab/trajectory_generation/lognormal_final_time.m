
% This function is an objective function for auxiliary optimization problem
% in order to find a (denoted alpha in the report). We specify tf and then
% we get an annulation of the velocity profile at the end of the spline. 

function err = lognormal_final_time(a)
global tf
t = tf;
dln = lognormal(t,1,a); % Lognormal computation
% Objective funtion : quadratic residual of deltalognormal we want to
% minimize this residual.
err = (10)^6*dln^2;
end