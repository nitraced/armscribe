function v = lognormal(t,A,a)

%% Parmetrization
% Sigmas
sigma = 1/2;
sigma1 = sigma;
sigma2 = sigma*1.2;
% Logdelays
mu1 = log(a);
mu2 = log(a);
% Delays
t01 = 0;
t02 = a/2;
% Relative amplitude
A2 = 0.3;

%% Delta lognormal function 
if t>t01
    lambda_1 = 1./(sigma1*sqrt(2*pi).*(t-t01)).*exp(-((log(t-t01)-mu1).^2)./(2*sigma1^2));
else
    lambda_1 = 0;
end

if t>t02
    lambda_2 = A2*1./(sigma2*sqrt(2*pi).*(t-t02)).*exp(-((log(t-t02)-mu2).^2)./(2*sigma2^2));
else
    lambda_2 = 0;
end
v = A*(lambda_1-lambda_2); % Deltalognormal value at tf for the given a parameter.

end