%% Simple linear stroke test

% 1 - Delta lognormal parameters computation
% Final time is global for optimizer
global tf
% initial coordinate scheduled :
stroke.p0 = [-0.3,0.3]';
% final coordinate scheduled
stroke.pf = [0.4,-0.4]';
% Objective mean speed
stroke.v_bar = 0.1; % 0.1 m/s 
% Time of stroke computation
stroke.l = norm(stroke.p0-stroke.pf);
tf = stroke.l/stroke.v_bar;
% Convenient a parameter
stroke.a = lsqnonlin(@lognormal_final_time,tf);
% Integral value
t_vec = linspace(0,tf,1000);
[v,integral]=lognormal_vec(t_vec,stroke.a);
% Multiplier coefficient
stroke.A = stroke.l/integral;

% Stroke definition
stroke.is_circle = 0;
stroke.theta_f = pi;

%% Sim
sim('testbench')

% %% Test
% % For test only, simulink wasn't working 
% dt = mean(diff(t_vec));
% curvilign_abscice=stroke.A*cumsum(stroke.v)*dt/l;
% 
% subplot(2,1,1)
% plot(t_vec,v)
% grid on
% title('Vitesse tangentielle')
% subplot(2,1,2)
% plot(t_vec,curvilign_abscice)
% grid on
% title('Ascisse curviligne')
% % 2 - Trajectory
% x_ref= zeros(2,length(curvilign_abscice));
% for i=1:length(curvilign_abscice)
%     x_ref(:,i)=(stroke.pf-stroke.p0).*curvilign_abscice(i);
% end
% figure
% plot(x_ref(1,:),x_ref(2,:),'.')