function d_lambda_1 = lambda_1_dot(a,t)
%LAMBDA_1_DOT
%    D_LAMBDA_1 = LAMBDA_1_DOT(A,T)

%    This function was generated by the Symbolic Math Toolbox version 8.1.
%    13-Nov-2019 15:21:38

t4 = log(a);
t5 = log(t);
t2 = t4-t5;
t3 = 1.0./t.^2;
t6 = t2.^2;
t7 = exp(t6.*-2.0);
d_lambda_1 = t3.*t7.*(-7.978845608028654e-1)+t2.*t3.*t7.*3.191538243211462;
