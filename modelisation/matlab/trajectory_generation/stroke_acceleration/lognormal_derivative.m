syms a t positive

%% Parmetrization
% Sigmas
sigma = 1/2;
sigma1 = sigma;
sigma2 = sigma*1.2;
% Logdelays
mu1 = log(a);
mu2 = log(a);
% Delays
t01 = 0;
t02 = a/2;
% Relative amplitude
A2 = 0.3;

% Lognormal symbolics
lambda_1 = 1./(sigma1*sqrt(2*pi).*(t-t01)).*exp(-((log(t-t01)-mu1).^2)./(2*sigma1^2));
lambda_2 = A2*1./(sigma2*sqrt(2*pi).*(t-t02)).*exp(-((log(t-t02)-mu2).^2)./(2*sigma2^2));

% Derivative of each lognormals
d_lambda_1 = diff(lambda_1,t);
d_lambda_2 = diff(lambda_2,t);

% Export to numerical functions diff_lambda_i(a,t)
diff_lambda_1 = matlabFunction(d_lambda_1,'File','lambda_1_dot');
diff_lambda_2 = matlabFunction(d_lambda_2,'File','lambda_2_dot');
