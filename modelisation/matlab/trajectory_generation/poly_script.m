%% Poly trajectory
clear
close all
global tf
poly.primitives_plot = 0;

poly.control_points = [
    -0.35 -0.35 -0.35 -0.1 -0.1 0.1 0.1 0.2 0.35 0.35 0.3 0.4;
    -0.2 0.4 0.1 0 -0.2 0 -0.2 -0.2 -0.2 -0.1 0 0
    ];
poly.control_points = poly.control_points + [0 -0.1]';
[unused,poly.number_of_points] = size(poly.control_points);

% Verification plot (where are the control points?)
if poly.primitives_plot
    % Draw limit circle
    radius = 0.6;
    theta = linspace(0,2*pi,200);
    x_circle = radius*cos(theta);
    y_circle = radius*sin(theta);
    plot(x_circle,y_circle,'k')
    hold on
    grid on
    for i=1:poly.number_of_points
        scatter(poly.control_points(1,i),poly.control_points(2,i))
        text(poly.control_points(1,i)+0.01,poly.control_points(2,i),num2str(i))
        set(gca,'xtick',[-10:0.1:10])
        set(gca,'ytick',[-10:0.1:10])
    end
end
%%
% Segment vector : initial point indice, final point indice, theta (0 if
% segment is linear), 1 if pen is writting
poly.segments = [
    1 2 0 1; %S1
    2 3 -pi 1; %A1
    3 4 0 0; %D1
    4 5 2*pi 1; %A1
    4 6 0 0; %D2
    6 7 0 1; %S2La paramétrisation utilisée pour cette trajectoire est décrite en \textbf{centimètres} dans le tableau ci-contre.
    7 8 0 1; %S4
    8 9 0 0; %D3
    9 10 0 1; %S5
    10 11 0 1; %S6
    11 12 0 0; %D4
    12 10 0 1; %S7
    ];

[poly.number_of_segments,unused] = size(poly.segments);
xz_gen.Data  = [];
xzd_gen.Data = [];
xz_gen.Time  = [0];
xzd_gen.Time = [0];
for i=1:poly.number_of_segments
    i
    p0 = poly.control_points(:,poly.segments(i,1));
    pf = poly.control_points(:,poly.segments(i,2));
    thetaf = poly.segments(i,3);
    % Stroke parameter generation
    if thetaf==0
        % Linear stroke
        stroke = stroke_gen(0.1,p0,pf);
    else
        % Circular stroke
        stroke = stroke_gen(0.1,p0,pf,thetaf);
    end
    % Stroke simulation
    sim('testbench',tf);
    % Full trajectory generation
    xz_gen.Time = [xz_gen.Time ; xz_des.Time + xz_gen.Time(end)];
    xz_gen.Data = [xz_gen.Data ; xz_des.Data];
    xzd_gen.Time = [xzd_gen.Time ; xzd_des.Time + xzd_gen.Time(end)];
    xzd_gen.Data = [xzd_gen.Data ; xzd_des.Data];
    % Data logging
    traj_des.positions{i}=p_des;
    traj_des.velocities{i}=p_dotdes;
    % Print trajectories
    if poly.primitives_plot
        if poly.segments(i,4)
            % If it's writting
            plot(p_des.signals.values(:,1),p_des.signals.values(:,2),'.')
        else
            % If it's not writting (wow)
            plot(p_des.signals.values(:,1),p_des.signals.values(:,2),'--')
        end
    end
end

xz_gen = timeseries(xz_gen.Data,xz_gen.Time(2:end));
xzd_gen = timeseries(xzd_gen.Data,xzd_gen.Time(2:end));

% % Print xz
% figure()
% grid on
% hold on
% title('Cartesian coordinates generated')
% xlim([-0.6 0.6]);
% ylim([-0.6 0.6]);
% plot(xz_gen.Data(:,1),xz_gen.Data(:,2),'--','LineWidth',1.5);
% legend('xz_{gen}')
%  xlabel('x [m]')
%  ylabel('z [m]')
% 
% % Print xzd
% figure()
% grid on
% hold on
% title('Cartesian speed generated')
% plot(xzd_gen.Time,xzd_gen.Data,'--','LineWidth',1.5);
% legend('xzd_{gen}')
% xlabel('Time')
% ylabel('xzd')
