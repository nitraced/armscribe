function [p,pdot]  = cartesian_stroke(lt,pf,p0,theta_f,is_circle,v)
if is_circle
    % If the stroke is a circle
    theta = lt*theta_f;
    R = [cos(theta),-sin(theta); sin(theta) cos(theta)];
    pc = (pf+p0)/2;
    p = pc + R*(p0-pc);
    % Compute the derivative
    radius = norm(pc-p0);
    Rdot = [-sin(theta),-cos(theta); cos(theta) -sin(theta)];
    etheta = Rdot*(p0-pc)/radius;
    pdot = theta_f*v*etheta/2;
else
    % If it's a line
    p = p0 + lt*(pf-p0);
    pdot = v*(pf-p0)/norm(pf-p0);
end
end
