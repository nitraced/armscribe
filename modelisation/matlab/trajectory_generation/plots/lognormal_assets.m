close all
clear all

t = linspace(0,10,5000);
for k=1:5
%% Trajectory generator
a = k;
dln = lognormal_vec(t,a);
% Sample length
tvec = linspace(0,15,200);
% Logdelay
a= k;
plot(t,dln)
grid on 
hold on

end
xlabel('Temps [s]','Interpreter','latex','FontSize',16)
ylabel('Vitesse [m/s]','Interpreter','latex','FontSize',16)
title({'Valeurs de $v(t)$ en fonction de $\alpha$'},'Interpreter','latex','FontSize',16)
legend({'$\alpha=1$','$\alpha=2$','$\alpha=3$','$\alpha=4$','$\alpha=5$'},'Interpreter','latex','FontSize',16)
print -depsc fig/v_fct_alpha.eps