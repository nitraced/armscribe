
function stroke = stroke_gen(vbar,p0,pf,theta_f) 

if nargin==3
    stroke.is_circle = 0;
    stroke.theta_f = 0; % Dummy value unused
else
    if nargin == 4
        stroke.is_circle = 1;
        stroke.theta_f = theta_f;
    else
        error("uncompatible number of arguments provided for the stroke function, noob")
    end
end
% 1 - Delta lognormal parameters computation
% Final time is global for optimizer
global tf
% initial coordinate scheduled :
stroke.p0 = p0;
% final coordinate scheduled
stroke.pf = pf;
% Objective mean speed
stroke.v_bar = vbar; %nominal 0.1 m/s 
% Time of stroke computation
stroke.l = norm(stroke.p0-stroke.pf);
tf = stroke.l/stroke.v_bar;
% Convenient a parameter
stroke.a = lsqnonlin(@lognormal_final_time,tf);
% Integral value
t_vec = linspace(0,tf,1000);
[unused,integral]=lognormal_vec(t_vec,stroke.a);
% Multiplier coefficient
stroke.A = stroke.l/integral;
end