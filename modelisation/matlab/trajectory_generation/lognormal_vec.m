% Function which returns the deltalognormal values 
% (without amplitude adjustement) and the integral to proceed to the
% amplitude calculum
function [dln_vec, dln_int,lambda1,lambda2] = lognormal_vec(t_vec,a)
%% Parameters
% Assuming that the sampling rate is constant thus we get the period
delta_t = mean(diff(t_vec));
% Variances
sigma = 1/2;
sigma1 = sigma;
sigma2 = sigma*1.2;
% Logdelay
mu1 = log(a);
mu2 = log(a);
% Time delay
t01 = 0;
t02 = a/2;
A2 = 0.3;

%% Lognormal generator
% Agonist move
del = -1;
i = 1;
% Neuronal delay computation
while del<0 
   i = i+1;
   del=t_vec(i)-t01;
end
t = t_vec(i:end);
lambda_1 = 1./(sigma1*sqrt(2*pi).*(t-t01)).*exp(-((log(t-t01)-mu1).^2)./(2*sigma1^2)); 
lambda_1 = [zeros(1,i-1) lambda_1];

% Antagonist move
del = -1;
i = 1;
% Neuronal delay computation
while del<0 
   i = i+1;
   del=t_vec(i)-t02;
end
t = t_vec(i:end);
lambda_2 = 1./(sigma2*sqrt(2*pi).*(t-t02)).*exp(-((log(t-t02)-mu2).^2)./(2*sigma2^2)); 
lambda_2 = A2*[zeros(1,i-1) lambda_2];

% Deltalognormal computation
dln_vec= lambda_1-lambda_2; 
%% Integral computation
dln_int = 0;
for i=1:length(dln_vec)
   dln_int = dln_int + delta_t*dln_vec(i); 
end
end