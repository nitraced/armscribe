function tau = RR_ID(q,qd,qdd)
q1 = q(1);
q2 = q(2);
qd1 = qd(1);
qd2 = qd(2);

H = INM(q2);
C = CCGD(q1,q2,qd1,qd2);

tau = H*qdd + C;                                         
end