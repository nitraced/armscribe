function qdd = RR_FD(q,qd,tau)
q1 = q(1);
q2 = q(2);
qd1 = qd(1);
qd2 = qd(2);

H = INM(q2);
C = CCGD(q1,q2,qd1,qd2);


qdd = H\(tau-C);                                         
end