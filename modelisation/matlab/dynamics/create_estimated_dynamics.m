%% Load robot
clear
RR = load_RR('num');
%% States definition
syms q1 q2 qd1 qd2 qdd1 qdd2 tau1 tau2 real
q = [q1, q2]';
qd = [qd1, qd2]';
qdd = [qdd1, qdd2]';
tau = [tau1, tau2]';
%% Add estimation error
error = 0.5;
RR.I{1} = estimated_I(RR.I{1},error);
RR.I{2} = estimated_I(RR.I{2},error);
%% Dynamic model
[H,C] = HandC( RR, q, qd );
% Ajout de frottement
C = C + [0.01*qd1
         0.01*qd2];
H = vpa(simplify(H),4)
C = vpa(simplify(C),4)
matlabFunction(H,'File','INM');
matlabFunction(C,'File','CCGD');