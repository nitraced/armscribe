function I = estimated_I(I,error)
% mass
I(4:6,4:6) = I(4:6,4:6) + rand(3,3,1)*norm(I(4:6,4:6))*error;
% inertia
I(1:3,1:3) = I(1:3,1:3) + rand(3,3,1)*norm(I(1:3,1:3))*error;
I(4:6,1:3) = I(4:6,1:3) + rand(3,3,1)*norm(I(4:6,1:3))*error;
I(1:3,4:6) = I(4:6,1:3)';
end

