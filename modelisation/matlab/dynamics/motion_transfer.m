function in1 = motion_transfer(in1,x)
    in1(4:6) = in1(4:6) + skew(x(4:6))*in1(1:3);
end

