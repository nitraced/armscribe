%% Load robot
clear
test_num = 1;
if test_num
RR = load_RR('num');
else
RR = load_RR('sym');
end
%% States definition
syms q1 q2 qd1 qd2 qdd1 qdd2 tau1 tau2 real
q = [q1, q2]';
qd = [qd1, qd2]';
qdd = [qdd1, qdd2]';
tau = [tau1, tau2]';
%% Full Dynamic model
[H,C] = HandC( RR, q, qd );
% Ajout de frottement
C = C + RR.d.*[qd1 % real = 0.01
              qd2]; % real = 0.01
H = vpa(simplify(H),4)
C = vpa(simplify(C),4)
if test_num
matlabFunction(H,'File','INM');
matlabFunction(C,'File','CCGD');
end 

%% G, D separately
% G
[~,G] = HandC( RR, q, qd*0 );
% D
D = RR.d.*[qd1 
           qd2]; 

% Save function
G = vpa(simplify(G),4);
D = vpa(simplify(D),4);
if test_num
matlabFunction(G,'File','get_G');
matlabFunction(D,'File','get_D');
end 

%% CC_mat separately
RR.gravity = RR.gravity*0;
qd_r = [1; 0];
[~,CC_1] = HandC_passivity( RR, q, qd, qd_r );
qd_r = [0; 1];
[~,CC_2] = HandC_passivity( RR, q, qd, qd_r );
CC = vpa(simplify([CC_1 CC_2]),4)
% test 1
test_1 = simplify(CC*qd + G + D - C)
% test 2
H
H_dot = vpa(simplify(CC + CC'),4)

% Save function
matlabFunction(CC,'File','get_CC');