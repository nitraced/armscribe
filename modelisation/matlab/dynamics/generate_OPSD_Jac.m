%% Numerical test
clear
load('case.mat')

% Kinematics
J = get_jacobian(q1,q2);
t1 = max(abs([[xd zd]' - J*[qd1 qd2]']))
Jd = get_jacobian_dot(q1,q2,qd1,qd2);
t2 = max(abs([[xdd zdd]' - (J*[qdd1 qdd2]'+Jd*[qd1 qd2]')]))

% Joint Space dynamics
H = INM(q2);
C = CCGD(q1,q2,qd1,qd2);
t3 = max(abs([tau          - RR_ID([q1; q2],[qd1; qd2],[qdd1; qdd2])]))
t4 = max(abs([[qdd1; qdd2] - RR_FD([q1; q2],[qd1; qd2],tau)]))

% Conversion
Lambda = inv(J/H*J');
Jt_gi = Lambda*J/H;
bt = Lambda*(J/H*C - Jd*[qd1;qd2]); 

% Fop = Ft
Fop = Lambda*[xdd zdd]' + bt;
Ft  = Jt_gi*tau;
t5 = max(abs([Fop-Ft]))
t6 = max(abs([tau - Jt_gi\Fop]))
test = max([t1 t2 t3 t4 t5 t6])

%% Function generation
syms q1 q2 qd1 qd2 xdd zdd real
% Kinematics
J = get_jacobian(q1,q2);
Jd = get_jacobian_dot(q1,q2,qd1,qd2);

% Joint Space dynamics
H = INM(q2);
C = CCGD(q1,q2,qd1,qd2);

% Conversion
Lambda = inv(J/H*J');
Jt_gi = Lambda*J/H;
bt = Lambda*(J/H*C - Jd*[qd1;qd2]); 

% Fop = Ft
Fop = Lambda*[xdd zdd]' + bt;
OPSD_torque = simplify(Jt_gi\Fop);
matlabFunction(OPSD_torque,'File','OPS_ID');

%% Final test
% clear
% load('case.mat')
% xdd = v(1);
% zdd = v(2);
% final_test = [tau OPS_ID(q1,q2,qd1,qd2,xdd,zdd)]