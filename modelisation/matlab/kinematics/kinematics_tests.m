clear
close all
load('case.mat')
%% Jacobians
J = get_jacobian(q1,q2);
t1 = [[xd zd]' J*[qd1 qd2]'] 
Jd = get_jacobian_dot(q1,q2,qd1,qd2);
t2 = [[xdd zdd]' J*[qdd1 qdd2]'+Jd*[qd1 qd2]']
%% FK
t3 = [[x; z] FK(q1,q2)]
%% IK
t4 = [[q1; q2] IK(x,z)]

%% Mesh
q1 = linspace(-pi,pi);
q2 = linspace(-pi,pi);
[Q1,Q2] = meshgrid(q1,q2);
SPAN = FK(Q1,Q2);
figure(1)
grid on
hold on
for i = 0:size(q2,2)-1
    scatter(SPAN(1+i,:),SPAN(size(q2,2)+1+i,:),6,'filled')
end
%% 1 dot test
x = 0;
z = 0.3;
Q = IK(x,z);
SPAN = FK(Q(1),Q(2));
figure(2)
scatter(0,0,15,'filled')
xlim([-0.6 0.6]);
ylim([-0.6 0.6])
grid on
hold on
scatter(x,z,'+')
scatter(SPAN(1),SPAN(2))