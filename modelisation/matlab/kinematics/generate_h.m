clear
syms q1 q2 qd1 qd2 xdd zdd real
RR = load_RR('num');
%% X
X(:,:,2,1) = (rotz(q1)*RR.XT(:,:,2,1));
X(:,:,3,2) = (rotz(q2)*RR.XT(:,:,3,2));
S = [0 0 1 0 0 0]';

%% h = Jtaskd*qd
qd = [qd1;qd2];
n = RR.nb;
h = sym(zeros(6,n));
vv = sym(zeros(6,n));
RR.parent = [0 1 2];
for i = 2:RR.nb
    lamb_i = RR.parent(i);
    vv(:,i)= X(:,:,i,lamb_i)*vv(:,lamb_i)     + S*qd(i-1);
    h(:,i) = X(:,:,i,lamb_i)*h(:,lamb_i) + crm(vv(:,i))*(S*qd(i-1));       
end
% Expressions dans la base fixe
h = (X(:,:,3,2)*X(:,:,2,1))\h(:,3);
h = simplify(h([4 6]))

%% J
clear q1 q2
syms q1(t) q2(t) real
q = [q1 q2];
S = [0 0 1 0 0 0]';
J = sym(zeros(6,2));

J(:,1)  = (rotz(q1)*RR.XT(:,:,2,1))\S;
J(:,2) = (rotz(q2)*RR.XT(:,:,3,2)*rotz(q1)*RR.XT(:,:,2,1))\S;
J = simplify(J([4 6], :));
%% Jd
Jd = simplify(diff(J,t));
syms qd1 qd2 real
Jd = subs(Jd,diff(q1(t), t),qd1);
Jd = subs(Jd,diff(q2(t), t),qd2);
syms t1 t2 real
Jd = subs(Jd,q1(t),t1);
Jd = subs(Jd,q2(t),t2);
syms q1 q2 real
Jd = subs(Jd,t1,q1);
Jd = subs(Jd,t2,q2);
Jd = simplify(Jd);
%%
simplify(Jd*qd)