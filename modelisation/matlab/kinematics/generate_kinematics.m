%% Inverse Kinematics
clear
clc
% Load RR
RR = load_RR('num');
% Calculate transforms
% Plucker Transforms
syms q1 q2 real
q = [q1 q2];
T = RR.base'*hom(rz(q1),zeros(3,1))*transl([RR.DH(1,3) 0 0])*hom(rz(q2),zeros(3,1))*transl([RR.DH(2,3) 0 0]);
% Given Tobj of the effector wrt the base
syms nx ax ox x ny ay oy y nz az oz z real
Tobj = [nx ax ox x
        ny ay oy y
        nz az oz z
        0  0  0  1]; 
    

%% FK
xz = simplify(T([1,3],4))
matlabFunction(xz,'File','FK');

%% IK
ik = simplify(T) == Tobj;
ik([1,3],4)
sol = solve(ik([1,3],4),[q1,q2]);
q1q2 = simplify([sol.q1(2);sol.q2(2)])
matlabFunction(q1q2,'File','IK');
