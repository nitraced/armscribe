%% Generate jacobian of End effector wrt base
clear
clc
RR = load_RR('sym');
% Plucker Transforms
syms q1 q2 real
q = [q1 q2];
S = [0 0 1 0 0 0]';
J = sym(zeros(6,2));

J(:,1)  = (rotz(q1)*RR.XT(:,:,2,1))\S;
J(:,2) = (rotz(q2)*RR.XT(:,:,3,2)*rotz(q1)*RR.XT(:,:,2,1))\S;

X = (RR.XT(:,:,4,3)*rotz(q2)*RR.XT(:,:,3,2)*rotz(q1)*RR.XT(:,:,2,1));
[E,r] = plux(X);
r = simplify(r);
J = xlt(r)*J; % r different from FK
J = simplify(J([4 6], :));
% Safe method
J = jacobian(FK(q1,q2));
matlabFunction(J,'File','get_jacobian');

%% Derivative of Jacobian
syms t q1(t) q2(t) real
J = get_jacobian(q1,q2) ;
Jd = simplify(diff(J,t));
syms qd1 qd2 real
Jd = subs(Jd,diff(q1(t), t),qd1);
Jd = subs(Jd,diff(q2(t), t),qd2);
syms t1 t2 real
Jd = subs(Jd,q1(t),t1);
Jd = subs(Jd,q2(t),t2);
syms q1 q2 real
Jd = subs(Jd,t1,q1);
Jd = subs(Jd,t2,q2);
Jd = Jd(0);
matlabFunction(Jd,'File','get_jacobian_dot');
