%% xz
figure()
grid on
hold on
title('Cartesian coordinates')
xlim([-0.6 0.6]);
ylim([-0.6 0.6])
plot(out.xz_des.Data(1,1),out.xz_des.Data(1,2),'O','LineWidth',1.5);
plot(out.xz.Data(:,1),out.xz.Data(:,2),'LineWidth',1.3);
plot(out.xz_des.Data(:,1),out.xz_des.Data(:,2),'--','LineWidth',1.5);
plot(out.xz_des.Data(end,1),out.xz_des.Data(end,2),'O','LineWidth',1.5);
legend('xz_{initial, des}','xz','xz_{des}','xz_{final, des}')
xlabel('x')
ylabel('z')

%% xzd(time)
figure()
grid on
hold on
title('Cartesian coordinates')
plot(out.tout,out.xzd_des.Data(:,:),'--','LineWidth',1.6);
plot(out.tout,out.xzd.Data(:,:),'LineWidth',1.6);
legend('xzd_{des}','xzd')
xlabel('t')
ylabel('vel')
%% Errors
figure()
title('Errors')
subplot(3,1,1)
hold on
grid on
plot(out.tout,vecnorm(out.xz.Data'-out.xz_des.Data'),'LineWidth',1.5);
legend('e')
xlabel('Time (s)')
ylabel('Position error')

subplot(3,1,2)
hold on
grid on
plot(out.tout,vecnorm(out.xzd.Data'-out.xzd_des.Data'),'LineWidth',1.5);
legend('ed')
xlabel('Time (s)')
ylabel('Velocity error')

subplot(3,1,3)
hold on
grid on
plot(out.tout,vecnorm(out.u.Data'),'LineWidth',1.5);
legend('u')
xlabel('Time (s)')
ylabel('Command')