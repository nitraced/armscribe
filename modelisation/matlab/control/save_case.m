%% out must be created
t = 50;
out.tout(t)
%%
q1 = out.q.Data(t,1);
q2 = out.q.Data(t,2);
qd1 = out.qd.Data(t,1);
qd2 = out.qd.Data(t,2);
qdd1 = out.qdd.Data(t,1);
qdd2 = out.qdd.Data(t,2);
x = out.xz.Data(t,1);
z = out.xz.Data(t,2);
xd = out.xzd.Data(t,1);
zd = out.xzd.Data(t,2);
thy = out.thy.Data(t);
wy = out.wy.Data(t);
wdy = out.wdy.Data(t);
xdd = out.xzdd.Data(t,1);
zdd = out.xzdd.Data(t,2);
tau = out.tau.Data(t,:)'; 
v   = out.v.Data(t,:)';
u   = out.u.Data(t,:)';
save case.mat q1 q2 qd1 qd2 qdd1 qdd2 x z xd zd thy wy wdy xdd zdd tau v u