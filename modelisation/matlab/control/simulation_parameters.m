%% Trajectory
% run testscript.m
run poly_script.m

%% Model to use for simulation
% 1 = mathematical forward dynamics
% 2 = simscape model
model_type = 2;

%% Controller
% 1 = Joint space motion control
% 2 = Task space motion control
% 3 = PID

control_scheme = 1;

if control_scheme == 1
    Kp = diag([400 400]);
    Kd = diag([30 30]);
    Ki = diag([1 1]*0.3);
elseif control_scheme == 2
    Kp = diag([50 50]);
    Kd = diag([30 30]);
    Ki = diag([1 1]);
elseif control_scheme == 3
    Kp = diag([20 20]);
    Kd = diag([1 1]);
    Ki = diag([0.1 0.1]);
end

%% MDO
lambda = 1;
nu = 0.001;