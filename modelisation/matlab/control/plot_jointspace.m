%% q
% figure()
% grid on
% title('Dynamic behaviour')
% hold on
% plot(out.tout,rad2deg(out.q.Data(:,:)),'LineWidth',1.5);
% plot(out.tout,rad2deg(out.q_des.Data(:,:)),'--','LineWidth',1.5);
% legend('q1','q2','q1_{des}','q2_{des}')
% xlabel('Time (s)')
% ylabel('Angles (deg)')

%% qd
% figure()
% grid on
% title('Dynamic behaviour')
% hold on
% plot(out.tout,rad2deg(out.qd.Data(:,:)),'LineWidth',1.5);
% plot(out.tout,rad2deg(out.qd_des.Data(:,:)),'--','LineWidth',1.5);
% legend('qd1','qd2','qd1_{des}','qd2_{des}')
% xlabel('Time (s)')
% ylabel('Angular vel (deg/s)')

%% xz
figure()
grid on
hold on
title('Cartesian coordinates')
xlim([-0.6 0.6]);
ylim([-0.6 0.6])
plot(out.xz.Data(:,1),out.xz.Data(:,2),'LineWidth',1.3);
plot(out.xz_des.Data(:,1),out.xz_des.Data(:,2),'--','LineWidth',1.5);
plot(out.xz_des.Data(end,1),out.xz_des.Data(end,2),'O','LineWidth',1.5);
legend('xz','xz_{des}','xz_{final, des}')
xlabel('x')
ylabel('z')
%% xzd(time)
% figure()
% grid on
% hold on
% title('Cartesian coordinates')
% plot(out.tout,out.xzd.Data(:,:),'LineWidth',1.6);
% plot(out.tout,out.xzd_des.Data(:,:),'--','LineWidth',1.6);
% legend('xzd','xzd_{des}')
% xlabel('t')
% ylabel('vel')
%% Errors
% figure()
% title('Errors')
% subplot(4,1,1)
% hold on
% grid on
% plot(out.tout,rad2deg(out.e.Data),'LineWidth',1.5);
% legend('e')
% xlabel('Time (s)')
% ylabel('Errors')
% 
% subplot(4,1,2)
% hold on
% grid on
% plot(out.tout,rad2deg(out.ed.Data),'LineWidth',1.5);
% legend('ed')
% 
% subplot(4,1,3)
% hold on
% grid on
% plot(out.tout,rad2deg(out.ei.Data),'LineWidth',1.5);
% legend('ei')
% 
% subplot(4,1,4)
% hold on
% grid on
% plot(out.tout,out.u.Data,'LineWidth',1.5);
% legend('u1','u2')