clear
close all
warning off
run simulation_parameters.m
out = sim('RR_testbench.slx',xz_gen.Time(end));
%% Plot
if control_scheme == 1
    plot_jointspace
elseif control_scheme == 2
    plot_taskspace
elseif control_scheme == 3
    plot_pid
end
