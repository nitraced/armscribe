fig1 = 1
fig2 = 2
L = findobj(fig1,'type','line');
copyobj(L,findobj(fig2,'type','axes'));
%% 
legend('xzd1_{des}','xzd2_{des}', 'xzd1_{fb lin}','xzd2_{fb lin}', 'xzd1_{PID}','xzd2_{PID}')
title('Cartesian velocity')
ylabel('velocity (m/s)')
xlabel('time (s)')