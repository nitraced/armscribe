function RR = load_RR(type)
% load_RR creates a variable RR with the physical properties of the RR
% robot using Featherstone's format. Either symbolically or numerically.
% Also display a representation of the robot.
    if type == 'num'
        %% Numerical data definition using Featherstone format
        % DH Parameters [d, a, alpha, theta]
            DH = [ 0 0 0.3 0 
                   0 0 0.3 0 ];
        % Inertia 
            % Arm 1
            m = 0.01; % real = 0.03
            c = [0.15 0 0]; % From referential
            J = diag([5e-07, 0.00022525, 0.00022525]); % Around G
            I1 = mcI(m,c,J);
            % Arm 2
            m = 0.01; % real = 0.03
            c = [0.15 0 0]; % From referential
            J = diag([5e-07, 0.00022525, 0.00022525]); % Around G
            I2 = mcI(m,c,J);
        % Gravity
            g = 9.8066;
        % Damping
        RR.d = [0 ; 0]; % real = [0.01 0.01]
        
        %% Plot using Corke Library
%         % Create 2 link
%         L(1) = Link(DH(1,:));
%         L(2) = Link(DH(1,:));
% 
%         % Create robot
%         robot = SerialLink(L, 'name', 'Robot');
%         robot.base = pluho(round(rotz(pi/2)*rotx(pi/2))');
%         RR.base = robot.base;
%         % Show
%         robot.plot([0 pi/4])
    
    elseif type == 'sym'
        %% Symbolic
        % DH Parameters [d, a, alpha, theta]
            syms a1 a2 real
            DH = [ 0 0 a1 0 
                   0 0 a2 0 ];
        % Inertia 
            % Arm 1
            syms m1 c1 J1x J1y J1z real
            m = m1;
            c = [c1 0 0]; % From referential
            J = diag([J1x J1y J1z]); % Around G
            I1 = mcI(m,c,J);
            % Arm 2
            syms m2 c2 J2x J2y J2z real
            m = m2;
            c = [c2 0 0]; % From referential
            J = diag([J2x J2y J2z]); % Around G
            I2 = mcI(m,c,J);
        % Gravity
            syms g real
        % Damping
            syms d1 d2
            RR.d = [d1 ; d2];
    else
        "Incorrect type, either 'num' or 'sym'"
    end
    RR.NB = 2;
    RR.nb = RR.NB+1;
    RR.i = 2;
    RR.parent = [0 1];
    RR.gravity = [0 0 -g]';
    RR.DH = DH;
    RR.I = {I1,I2};
    % Joint model
    RR.jtype  = repmat({'Rz'},1,RR.NB); 
    % Geometry X_T
    RR.base = round(hom(rz(pi/2),[0 0 0]'))*round(hom(rx(pi/2),[0 0 0]'));
    RR.Xtree = {round(rotz(pi/2)*rotx(pi/2)), xlt([DH(1,3) 0 0])};
    RR.XT = sym(zeros(6,6,3));
    RR.XT(:,:,2,1) = RR.Xtree{1};
    RR.XT(:,:,3,2) = RR.Xtree{2};
    RR.XT(:,:,4,3) = xlt([DH(2,3) 0 0]);
end