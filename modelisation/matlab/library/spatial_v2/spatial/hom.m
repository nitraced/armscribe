function  out = hom( E,r )

% Return the homogenous transform composed of E and r.
% Formulae:
%   X = [  E   0 ]   T = [ E  -Er ]
%       [ -Erx E ]       [ 0   1  ]
    
    out = [ E, -E*r; 0 0 0 1 ];
end